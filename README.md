# AirGradient to Prometheus

## Requirements
- Python
- Python Flask lib
- Python prometheus_client lib
- Python json lib
- Python threading lib
- Python argparse lib

## Getting started

My first try was to override api.airgradient.com in DNS but it seems the IP (165.22.51.108) is hardcoded. I suggest you setup a DNAT rule in your firewall to direct all the HTTP traffic to a reverse proxy host. Create vhost for api.airgradient.com with reverse proxy rules that will send the traffic over to this script. In the vhost definition make a location for every device you have. Here's an example nginx definition with two AirGradient One v9: 

```nginx
# api.airgradient.com
# Video surveillance Libcom
server {
    listen      80;
    listen      [::]:80;
    server_name api.airgradient.com;
    access_log /var/log/nginx/api.airgradient.com_access.log;
    error_log /var/log/nginx/api.airgradient.com_error.log error;

    location /sensors/airgradient:84fce611aaf0/measures {
      proxy_redirect off;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forward-For $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header Connection $http_connection;
      proxy_pass http://[::1]:5001;
      add_header X-Frame-Options "SAMEORIGIN";
    }
    location /sensors/airgradient:84fce60d6bf1/measures {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forward-For $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header Connection $http_connection;
      proxy_pass http://[::1]:5000;
      add_header X-Frame-Options "SAMEORIGIN";
    }
    location / {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forward-For $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header Connection $http_connection;
      return 404;
      add_header X-Frame-Options "SAMEORIGIN";
    }
}
```

Run the script correspondingly to the ports set in the reverse proxy's location. Here are two examples:

```bash
python3 airgradient_working_variables.py --path /sensors/airgradient:84fce60d6bf1/measures --receiver-port 5000 --prometheus-port 9190
python3 airgradient_working_variables.py --path /sensors/airgradient:84fce611aaf0/measures --receiver-port 5001 --prometheus-port 9191
```
Make sure your firewall settings are correct. Now you can scrape from the host this script will run on, like in this example from port 9190 and 9191.

## Prometheus / Grafana integration
In prometheus I scrape like this:
```yaml
scrape_configs:
  - job_name: 'airgradient'
    static_configs:
      - targets: [ '10.10.133.11:9190' ]
        labels:
          Raum: "Wohnzimmer"
      - targets: [ '10.10.133.11:9191' ]
        labels:
          Raum: "Keller"
    scheme: http
    metrics_path: '/metrics'
```
I've made my own grafana dashboard which is not very complicated. I set a variable for `instance` with the label `Raum` which is german for room.

## License
Copyright (c) 2024 Jochen Demmer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
