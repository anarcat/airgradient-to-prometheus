from flask import Flask, request, jsonify
from prometheus_client import start_http_server, Gauge
import json
import threading
import argparse
    
parser = argparse.ArgumentParser(description='Receive AirGradient data via http/json and serve it as scrapable prometheus data')
# Add command-line arguments
parser.add_argument('--prometheus-port', type=int, default=9090, help='Prometheus port (default: 9090)')
parser.add_argument('--receiver-port', type=int, default=5000, help='Receiver port (default: 5000)')
parser.add_argument('--path', type=str, required=True, help='URL path parameter (required)')

args = parser.parse_args()
prometheus_port = args.prometheus_port
receiver_port = args.receiver_port
path = args.path

app = Flask(__name__)

# Create Prometheus metrics
wifi_metric = Gauge('airquality_wifi', 'WiFi Signal Strength')
rco2_metric = Gauge('airquality_rco2', 'CO2 Concentration')
pm01_metric = Gauge('airquality_pm01', 'PM 0.1 Concentration')
pm02_metric = Gauge('airquality_pm02', 'PM 2.5 Concentration')
pm10_metric = Gauge('airquality_pm10', 'PM 10 Concentration')
pm003_metric = Gauge('airquality_pm003', 'PM 0.3 Count')
tvoc_metric = Gauge('airquality_tvoc', 'TVOC Index')
nox_metric = Gauge('airquality_nox', 'NOx Index')
atmp_metric = Gauge('airquality_atmp', 'Temperature')
rhum_metric = Gauge('airquality_rhum', 'Relative Humidity')

@app.route(path, methods=['POST'])
def receive_data():
    try:
        data = request.json

        # Update Prometheus metrics
        wifi_metric.set(data['wifi'])
        rco2_metric.set(data['rco2'])
        pm01_metric.set(data['pm01'])
        pm02_metric.set(data['pm02'])
        pm10_metric.set(data['pm10'])
        pm003_metric.set(data['pm003_count'])
        tvoc_metric.set(data['tvoc_index'])
        nox_metric.set(data['nox_index'])
        atmp_metric.set(data['atmp'])
        rhum_metric.set(data['rhum'])

        return jsonify({"status": "success"})
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

if __name__ == '__main__':
    # Start Flask server in a separate thread
    flask_thread = threading.Thread(target=app.run, kwargs={'host': '::1', 'port': receiver_port})
    flask_thread.start()

    # Start Prometheus metrics server
    start_http_server(prometheus_port)
